import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { EsbAppDetailsComponent } from './components/esb-app-details/esb-app-details.component';


const routes: Routes = [
  {
    path: 'appDetails',
    component: EsbAppDetailsComponent
  },
  {
    path: '',
    component: DashboardComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
