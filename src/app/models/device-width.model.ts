export enum DeviceWidth {
    XS = 'EXTRA-SMALL', // less thaqt 768 px
    SM = 'SMALL', // less than 993 px
    MD = 'MEDIUM', // less than 1200 px
    LG = 'LARGE' // greater than 1200 px
}
