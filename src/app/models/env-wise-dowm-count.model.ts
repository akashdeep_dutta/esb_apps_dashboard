export class EnvWiseDowmCount {
    public dayWise: EnvDownCount;
    public weekWise: EnvDownCount;
    public monthWise: EnvDownCount;
}

interface EnvDownCount {
    dev: number;
    qa: number;
    uat: number;
    prod: number;
}
