import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { DashboardService } from '../../services/dashboard.service';
import { BehaviorSubject } from 'rxjs';
//import {Post} from '../../model/Post';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})



export class DashboardComponent implements OnInit, OnDestroy {
  //public posts: Post[] = [];
  //public postDataSource: BehaviorSubject<Post[]>;
  public displayedColumns: string[] = ['id','userId', 'title', 'body'];
  private _subscrition: any;

  constructor(private _router: Router, private _dashboardService: DashboardService) {
    
  }

  ngOnInit(): void {
    /*
    this._subscrition = this._dashboardService.getPosts().subscribe(
      (response) => {
        this.posts = Array.from(response);
        this.postDataSource = new BehaviorSubject<Post[]>(this.posts);
        console.log(this.posts);
      },
      (error) => {
        console.log(error);
      }
    );
    */
  }

  ngOnDestroy(): void {
    if(this._subscrition){
      this._subscrition.unsubscribe();
    }
  }

  goToDetailsPage(id: number) {
    console.log('Application ID - '+ id);
    this._router.navigate(['/appDetails']);
  }
}
