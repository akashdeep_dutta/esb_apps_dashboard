import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsbAppListComponent } from './esb-app-list.component';

describe('EsbAppListComponent', () => {
  let component: EsbAppListComponent;
  let fixture: ComponentFixture<EsbAppListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsbAppListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsbAppListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
