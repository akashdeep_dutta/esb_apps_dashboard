import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopAppsComponent } from './top-apps.component';

describe('TopAppsComponent', () => {
  let component: TopAppsComponent;
  let fixture: ComponentFixture<TopAppsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopAppsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopAppsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
