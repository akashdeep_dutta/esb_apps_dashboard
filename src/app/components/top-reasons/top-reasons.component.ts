import { Component, OnInit } from '@angular/core';
import { TopReason } from '../../models/top-reason.model';

@Component({
  selector: 'app-top-reasons',
  templateUrl: './top-reasons.component.html',
  styleUrls: ['./top-reasons.component.scss']
})
export class TopReasonsComponent implements OnInit {
  topReasons: TopReason[];

  constructor() { 
    this.topReasons = TOP_REASONS;
  }

  ngOnInit(): void {
  }

}

const TOP_REASONS: TopReason[] = [
  {heading: 'Downstream Application is down',details: 'Downstream Application is down',occurance: 16},
  {heading: 'Operating System got crashed',details: 'Operating System got crashed',occurance: 14},
  {heading: 'Scheduled Maintainance',details: 'Scheduled Maintainance',occurance: 11},
  {heading: 'Internal Server Error',details: 'Internal Server Error',occurance: 8},
  {heading: 'SSL certificate got expired',details: 'SSL certificate got expired',occurance: 7}
];
