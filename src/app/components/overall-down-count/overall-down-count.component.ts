import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';

import { DashboardService } from '../../services/dashboard.service';
import { OverallDownCount } from '../../models/overall-down-count.model';
import { MatButtonToggleChange } from '@angular/material/button-toggle';
import { Subscription } from 'rxjs';
import { DeviceWidth } from 'src/app/models/device-width.model';

@Component({
  selector: 'app-overall-down-count',
  templateUrl: './overall-down-count.component.html',
  styleUrls: ['./overall-down-count.component.scss']
})
export class OverallDownCountComponent implements OnInit, AfterViewInit, OnDestroy {

  public selectedView: string = 'daily';
  public lineChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' }
  ];
  public lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: true
  };
  public lineChartColors: Color[] = [
    {
      borderColor: 'black',
      borderWidth: 1,
      backgroundColor: 'rgba(255,0,0,0.3)',
    },
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;
  private _deviceWidthSubscription: Subscription;
  public lineChartContainerCls: string = '';

  constructor(private _dashboardService: DashboardService) { 
  }

  ngOnInit() {
    this.setDayWiseDownCount();
    this._deviceWidthSubscription = this._dashboardService.deviceWidth$.subscribe( (deviceWidth: DeviceWidth) => {
      this.resizeChart(deviceWidth);
    });
  }

  ngOnDestroy(): void {
    this._deviceWidthSubscription ? this._deviceWidthSubscription.unsubscribe() : null;
  }

  ngAfterViewInit(){
    
  }

  private resizeChart(deviceWidth: DeviceWidth) {
    if(deviceWidth === DeviceWidth.XS){
      this.lineChartOptions.maintainAspectRatio = false;
      this.lineChartContainerCls = 'line-chart-container line-chart-container-mob';
    }
    else {
      this.lineChartOptions.maintainAspectRatio = true;
      this.lineChartContainerCls = 'line-chart-container';
    }
    this.chart.update();
  }

  setDayWiseDownCount(){
    this.lineChartData = [
      { data: [...OVERALL_DWON_COUNT.dayWise], label: 'Last five days Down Count' }
    ];
    this.lineChartLabels = this._dashboardService.getLasDays(5);
    this.chart.update();
  }

  setWeekWiseDownCount(){
    this.lineChartData = [
      { data: [...OVERALL_DWON_COUNT.weekWise], label: 'Last five weeks Down Count' }
    ];
    this.lineChartLabels = this._dashboardService.getLastWeeks(5);
    this.chart.update();
  }

  setMonthWiseDownCount(){
    this.lineChartData = [
      { data: [...OVERALL_DWON_COUNT.monthWise], label: 'Last five months Down Count' }
    ];
    this.lineChartLabels = this._dashboardService.getLastMonths(5);
    this.chart.update();
  }

  chartHovered(event) {

  }

  chartClicked(event) {
    
  }

  onSelectedViewChange(viewChangedEvent: MatButtonToggleChange) {
    if(this.selectedView !== viewChangedEvent.value){
      this.selectedView = viewChangedEvent.value;
      switch(this.selectedView) {
        case 'daily':
          this.setDayWiseDownCount();
          break;
        case 'weekly':
          this.setWeekWiseDownCount();
          break;
        case 'monthly':
          this.setMonthWiseDownCount();
          break;
        default:
          break;
      }
    }
  }

}

const OVERALL_DWON_COUNT: OverallDownCount = {
  dayWise: [5,1,7,3,4],
  weekWise: [14, 30, 15, 25, 5],
  monthWise: [40, 60, 54, 87, 32]
}