import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Observable, BehaviorSubject } from 'rxjs';
import { distinctUntilChanged, tap } from 'rxjs/operators';

import { DeviceWidth } from '../models/device-width.model';

@Injectable({
  providedIn: 'root'
})

export class DashboardService {
    constructor(private _httpClient: HttpClient){}

    private readonly MONTH_LIST: string[] = ['January','February','March','April','May','June','July','August','September','October','November','December'];
    private readonly WEEK_LIST: string[] = ['Week 1','Week 2','Week 3','Week 4','Week 5','Week 6','Week 7'];
    private readonly DAY_LIST: string[] = ['Sunday','Monday','Tuesday','Wednesday','Thirsday','Friday','Saturday'];

    private deviceWidthSubject: BehaviorSubject<DeviceWidth> = new BehaviorSubject<DeviceWidth>(DeviceWidth.LG);
    deviceWidth$ : Observable<DeviceWidth> = this.deviceWidthSubject.asObservable().pipe(
      distinctUntilChanged(),
      tap((width: DeviceWidth) => {
        console.log("The device width is -> " + width)
      })
    );

    public setDeviceWidth(width: number) {
      if(width >= 1200) {
        this.deviceWidthSubject.next(DeviceWidth.LG);
      }
      else if(width > 992) {
        this.deviceWidthSubject.next(DeviceWidth.MD);
      }
      else if(width > 767) {
        this.deviceWidthSubject.next(DeviceWidth.SM);
      }
      else {
        this.deviceWidthSubject.next(DeviceWidth.XS);
      }
    }

    public getPosts(): Observable<any>{
      return this._httpClient.get('https://jsonplaceholder.typicode.com/posts');
    }

    public getLastMonths(n: number): string[] {
      let last_n_months: string[] = [];
      let today: Date = new Date();
      for(let i=0; i<n ; i++){
        last_n_months.push(this.MONTH_LIST[today.getMonth()]+ ' , ' + today.getFullYear().toString());
        today.setMonth(today.getMonth() - 1)
      }
      return last_n_months;
    }

    public getLastWeeks(n: number): string[] {
      return [...this.WEEK_LIST].slice(0 , n);
    }

    public getLasDays(n: number): string[] {
      let last_n_days: string[] = [];
      let today: Date = new Date();
      for(let i=0; i<n ; i++){
        let newDate: Date = new Date(today.setDate(today.getDate() - 1));
        last_n_days.push(this.DAY_LIST[newDate.getDay()]);
      }
      return last_n_days;
    }
}