import { Component, OnInit, HostListener } from '@angular/core';

import { DeviceWidth } from './models/device-width.model';
import { DashboardService } from './services/dashboard.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit{
  title = 'esb-apps-dashboard';

  constructor(private _dashboardService: DashboardService) {

  }

  ngOnInit(): void {
    this._dashboardService.setDeviceWidth(window.innerWidth);
     //console.log("Device width -> "+deviceWidth);
  }

  @HostListener( 'window:resize', ['$event'])
  onResize(event) {
    this._dashboardService.setDeviceWidth(window.innerWidth);
    //console.log("Device width after resizing -> "+window.innerWidth);
  }

}
